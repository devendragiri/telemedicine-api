namespace SmaerChef.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Test")]
    public partial class Test
    {
        public int Id { get; set; }

        public int ServiceId { get; set; }

        [Column("Test")]
        [Required]
        [StringLength(100)]
        public string Test1 { get; set; }

        public virtual Service Service { get; set; }
    }
}
