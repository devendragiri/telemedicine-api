namespace SmaerChef.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FeeCharge")]
    public partial class FeeCharge
    {
        public int Id { get; set; }

        public int DoctorId { get; set; }

        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }

        public virtual Doctor Doctor { get; set; }
    }
}
