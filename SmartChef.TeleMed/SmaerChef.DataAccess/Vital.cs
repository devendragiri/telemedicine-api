namespace SmaerChef.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Vital
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public DateTime? RecentRecordedDate { get; set; }

        [StringLength(50)]
        public string BP { get; set; }

        [StringLength(50)]
        public string Sugar { get; set; }

        [StringLength(50)]
        public string Height { get; set; }

        [StringLength(50)]
        public string Weight { get; set; }

        public virtual Patient Patient { get; set; }
    }
}
