namespace SmaerChef.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PatientRepresentative")]
    public partial class PatientRepresentative
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(50)]
        public string RelationshipWithPatient { get; set; }

        public int PatientId { get; set; }

        [StringLength(50)]
        public string ContactNumber { get; set; }


        [StringLength(100)]
        public string Email { get; set; }

        public virtual Patient Patient { get; set; }
    }
}
