namespace SmaerChef.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Medication")]
    public partial class Medication
    {
        public int Id { get; set; }

        public int ServiceId { get; set; }

        [StringLength(100)]
        public string Medicine { get; set; }

        public int? TotalQuantity { get; set; }

        [StringLength(100)]
        public string PerStrength { get; set; }

        [StringLength(50)]
        public string Dose { get; set; }

        public virtual Service Service { get; set; }
    }
}
