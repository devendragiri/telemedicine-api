namespace SmaerChef.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsultaionLog")]
    public partial class ConsultaionLog
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public int DoctorId { get; set; }

        public DateTime? LoginDateTime { get; set; }

        public DateTime? ConsultationInDateTime { get; set; }

        public DateTime? ConsultationOutDateTime { get; set; }

        public virtual Doctor Doctor { get; set; }

        public virtual Patient Patient { get; set; }
    }
}
