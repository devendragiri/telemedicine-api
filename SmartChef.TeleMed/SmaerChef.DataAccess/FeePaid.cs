namespace SmaerChef.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FeePaid")]
    public partial class FeePaid
    {
        public int Id { get; set; }

        public int ServiceId { get; set; }

        [Column(TypeName = "money")]
        public decimal? AmountPaid { get; set; }

        public virtual Service Service { get; set; }
    }
}
