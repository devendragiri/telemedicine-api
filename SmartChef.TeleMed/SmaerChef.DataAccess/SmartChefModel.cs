namespace SmaerChef.DataAccess
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SmartChefContext : DbContext
    {
        public SmartChefContext()
            : base("name=SmartChefContext")
        {
        }

        public virtual DbSet<ConsultaionLog> ConsultaionLogs { get; set; }
        public virtual DbSet<Doctor> Doctors { get; set; }
        public virtual DbSet<DoctorProfile> DoctorProfiles { get; set; }
        public virtual DbSet<FeeCharge> FeeCharges { get; set; }
        public virtual DbSet<FeePaid> FeePaids { get; set; }
        public virtual DbSet<Medication> Medications { get; set; }
        public virtual DbSet<Patient> Patients { get; set; }
        public virtual DbSet<PatientRepresentative> PatientRepresentatives { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Test> Tests { get; set; }
        public virtual DbSet<Vital> Vitals { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Doctor>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Gender)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.ContactNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Specialization)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.HighestQualification)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .HasMany(e => e.ConsultaionLogs)
                .WithRequired(e => e.Doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Doctor>()
                .HasMany(e => e.DoctorProfiles)
                .WithRequired(e => e.Doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Doctor>()
                .HasMany(e => e.FeeCharges)
                .WithRequired(e => e.Doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Doctor>()
                .HasMany(e => e.Services)
                .WithRequired(e => e.Doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DoctorProfile>()
                .Property(e => e.Review)
                .IsUnicode(false);

            modelBuilder.Entity<FeeCharge>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<FeePaid>()
                .Property(e => e.AmountPaid)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Medication>()
                .Property(e => e.Medicine)
                .IsUnicode(false);

            modelBuilder.Entity<Medication>()
                .Property(e => e.PerStrength)
                .IsUnicode(false);

            modelBuilder.Entity<Medication>()
                .Property(e => e.Dose)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.Gender)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.ContactNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .HasMany(e => e.ConsultaionLogs)
                .WithRequired(e => e.Patient)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                .HasMany(e => e.DoctorProfiles)
                .WithRequired(e => e.Patient)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                .HasMany(e => e.PatientRepresentatives)
                .WithRequired(e => e.Patient)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                .HasMany(e => e.Services)
                .WithRequired(e => e.Patient)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                .HasMany(e => e.Vitals)
                .WithRequired(e => e.Patient)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PatientRepresentative>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<PatientRepresentative>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<PatientRepresentative>()
                .Property(e => e.RelationshipWithPatient)
                .IsUnicode(false);

            modelBuilder.Entity<PatientRepresentative>()
                .Property(e => e.ContactNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.FeePaids)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.Medications)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.Tests)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Test>()
                .Property(e => e.Test1)
                .IsUnicode(false);

            modelBuilder.Entity<Vital>()
                .Property(e => e.BP)
                .IsUnicode(false);

            modelBuilder.Entity<Vital>()
                .Property(e => e.Sugar)
                .IsUnicode(false);

            modelBuilder.Entity<Vital>()
                .Property(e => e.Height)
                .IsUnicode(false);

            modelBuilder.Entity<Vital>()
                .Property(e => e.Weight)
                .IsUnicode(false);
        }
    }
}
