﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class MedicationController : ApiController
    {
        
        // GET api/Medications/5
        public IHttpActionResult Get(int serviceId)
        {
            var db = new SmartChefContext();
            var query = db.Medications.Where(x => x.ServiceId == serviceId).ToList();
            return Ok(query);
        }

         
        // PUT api/Medications/JSON
        public void Put(Medication medication)
        {
            var db = new SmartChefContext();
            var query = db.Medications.Where(x => x.Id == medication.Id).FirstOrDefault();
            if (query == null)
                db.Medications.Add(medication);
            else
            {
                query.ServiceId = medication.ServiceId;
                query.Medicine = medication.Medicine;
                query.TotalQuantity = medication.TotalQuantity;
                query.PerStrength = medication.PerStrength;
                query.Dose = medication.Dose;
            }
            db.SaveChanges();
        }

        // DELETE api/Medications/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var medication = new Medication { Id = id };
            ctx.Medications.Attach(medication);
            ctx.Medications.Remove(medication);
            ctx.SaveChanges();

        }
    }
}
