﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmaerChef.DataAccess;
namespace SmartChef.TeleMed.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            
            using (var db = new SmartChefContext())
            {
                 

                // Display all Blogs from the database
                var query = from b in db.Patients
                            orderby b.FirstName
                            select b;

                 
                foreach (var item in query)
                {
                    ViewBag.Title = item.FirstName+" " +item.LastName; 
                }

               
            }
            return View();
        }
    }
}
