﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class ServiceController : ApiController
    {
        
        // GET api/Services/5
        public IHttpActionResult Get(int patientId)
        {
            var db = new SmartChefContext();
            var query = db.Services.Where(x => x.PatientId == patientId).ToList();
            return Ok(query);
        }

         
        // PUT api/Services/JSON
        public void Put(Service service)
        {
            var db = new SmartChefContext();
            var query = db.Services.Where(x => x.Id == service.Id).FirstOrDefault();
            if (query == null)
                db.Services.Add(service);
            else
            {
                query.DoctorId= service.DoctorId;
                query.PatientId = service.PatientId;
                query.Comments = service.Comments;
                query.ConsultedDate = service.ConsultedDate;
            }
            db.SaveChanges();
        }

        // DELETE api/Services/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var service = new Service { Id = id };
            ctx.Services.Attach(service);
            ctx.Services.Remove(service);
            ctx.SaveChanges();

        }
    }
}
