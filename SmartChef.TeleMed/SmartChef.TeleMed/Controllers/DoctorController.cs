﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
    //  [Authorize]
    public class DoctorController : ApiController
    {
        // GET api/Doctor
        public IHttpActionResult  Get()
        {
            var db = new SmartChefContext();
            var query = db.Doctors.ToList();

            return Ok(query);
        }

        // GET api/Doctor/5
        public IHttpActionResult Get(int Id)
        {
            var db = new SmartChefContext();
            var query = db.Doctors.Where(x => x.Id == Id).FirstOrDefault();
            
            return Ok(query);
        }

        // POST api/Doctor
        public IHttpActionResult Post(Doctor  doctor)
        {
            var db = new SmartChefContext();
            var query = db.Doctors.Where(x => x.FirstName == doctor.FirstName && x.LastName == doctor.LastName).FirstOrDefault();
            if (query == null)
                return NotFound();

            return Ok(query);
        }
        // PUT api/Doctor/JSON
        public void Put(Doctor doctor)
        {
            var db = new SmartChefContext();
            var query = db.Doctors.Where(x => x.Id == doctor.Id).FirstOrDefault();
            if (query == null)
                db.Doctors.Add(doctor);
            else
            {
                query.Address = doctor.Address;
                query.City = doctor.City;
                query.ContactNumber = doctor.ContactNumber;
                query.Country = doctor.Country;
                query.Email = doctor.Email;
                query.FirstName = doctor.FirstName;
                query.Gender = doctor.Gender;
                query.HighestQualification = doctor.HighestQualification;
                query.LastName = doctor.LastName;
            }
            db.SaveChanges();
        }

        // DELETE api/Doctor/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var doctor = new Doctor { Id = id };
            ctx.Doctors.Attach(doctor);
            ctx.Doctors.Remove(doctor);
            ctx.SaveChanges();
        }
    }
}
