﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class FeePaidController : ApiController
    {
        
        // GET api/FeePaids/5
        public IHttpActionResult Get(int serviceId)
        {
            var db = new SmartChefContext();
            var query = db.FeePaids.Where(x => x.ServiceId == serviceId).ToList();
            return Ok(query);
        }

         
        // PUT api/FeePaids/JSON
        public void Put(FeePaid feePaid)
        {
            var db = new SmartChefContext();
            var query = db.FeePaids.Where(x => x.Id == feePaid.Id).FirstOrDefault();
            if (query == null)
                db.FeePaids.Add(feePaid);
            else
            {
                query.ServiceId= feePaid.ServiceId;
                query.AmountPaid = feePaid.AmountPaid; 
            }
            db.SaveChanges();
        }

        // DELETE api/FeePaids/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var feePaid = new FeePaid { Id = id };
            ctx.FeePaids.Attach(feePaid);
            ctx.FeePaids.Remove(feePaid);
            ctx.SaveChanges();

        }
    }
}
