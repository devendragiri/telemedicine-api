﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class ConsultaionLogController : ApiController
    {
        
        // GET api/ConsultaionLogs/5
        public IHttpActionResult Get(int patientId)
        {
            var db = new SmartChefContext();
            var query = db.ConsultaionLogs.Where(x => x.PatientId == patientId).ToList();
            return Ok(query);
        }

         
        // PUT api/ConsultaionLogs/JSON
        public void Put(ConsultaionLog consultaionLog)
        {
            var db = new SmartChefContext();
            var query = db.ConsultaionLogs.Where(x => x.Id == consultaionLog.Id).FirstOrDefault();
            if (query == null)
                db.ConsultaionLogs.Add(consultaionLog);
            else
            {
                query.DoctorId= consultaionLog.DoctorId;
                query.PatientId = consultaionLog.PatientId;
                query.LoginDateTime = consultaionLog.LoginDateTime;
                query.ConsultationInDateTime = consultaionLog.ConsultationInDateTime;
                query.ConsultationOutDateTime = consultaionLog.ConsultationOutDateTime;
            }
            db.SaveChanges();
        }

        // DELETE api/ConsultaionLogs/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var consultaionLog = new ConsultaionLog { Id = id };
            ctx.ConsultaionLogs.Attach(consultaionLog);
            ctx.ConsultaionLogs.Remove(consultaionLog);
            ctx.SaveChanges();

        }
    }
}
