﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class DoctorProfileController : ApiController
    {
        
        // GET api/DoctorProfiles/5
        public IHttpActionResult Get(int patientId)
        {
            var db = new SmartChefContext();
            var query = db.DoctorProfiles.Where(x => x.PatientId == patientId).ToList();
            return Ok(query);
        }

         
        // PUT api/DoctorProfiles/JSON
        public void Put(DoctorProfile doctorProfile)
        {
            var db = new SmartChefContext();
            var query = db.DoctorProfiles.Where(x => x.Id == doctorProfile.Id).FirstOrDefault();
            if (query == null)
                db.DoctorProfiles.Add(doctorProfile);
            else
            {
                query.DoctorId= doctorProfile.DoctorId;
                query.PatientId = doctorProfile.PatientId;
                query.Rating = doctorProfile.Rating;
                query.Review = doctorProfile.Review;
            }
            db.SaveChanges();
        }

        // DELETE api/DoctorProfiles/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var doctorProfile = new DoctorProfile { Id = id };
            ctx.DoctorProfiles.Attach(doctorProfile);
            ctx.DoctorProfiles.Remove(doctorProfile);
            ctx.SaveChanges();

        }
    }
}
