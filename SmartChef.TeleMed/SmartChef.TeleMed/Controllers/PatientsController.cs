﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class PatientsController : ApiController
    {
        // GET api/Patients
        public IHttpActionResult Get()
        {
            var db = new SmartChefContext();
            var query = db.Patients.ToList();
            return Ok(query);
        }

        // GET api/Patients/5
        public IHttpActionResult Get(int Id)
        {
            var db = new SmartChefContext();
            var query = db.Patients.Where(x => x.Id == Id).FirstOrDefault();

            return Ok(query);
        }

        // POST api/Patients
        public IHttpActionResult Post(Patient patient)
        {
            var db = new SmartChefContext();
            var query = db.Patients.Where(x => x.FirstName == patient.FirstName && x.LastName == patient.LastName).FirstOrDefault();
            if (query == null)
                return NotFound();

                return Ok(query);
        }
        // PUT api/Patients/JSON
        public void Put(Patient patient)
        {
            var db = new SmartChefContext();
            var query = db.Patients.Where(x => x.Id == patient.Id).FirstOrDefault();
            if (query == null)
                db.Patients.Add(patient);
            else
            {
                query.Address = patient.Address;
                query.City = patient.City;
                query.ContactNumber = patient.ContactNumber;
                query.Country = patient.Country;
                query.Email = patient.Email;
                query.FirstName = patient.FirstName;
                query.Gender = patient.Gender;
                query.LastName = patient.LastName;
            }
            db.SaveChanges();
        }

        // DELETE api/Patients/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var patient = new Patient { Id = id };
            ctx.Patients.Attach(patient);
            ctx.Patients.Remove(patient);
            ctx.SaveChanges();

        }
    }
}
