﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class PatientRepresentativeController : ApiController
    {
        
        // GET api/PatientRepresentatives/5
        public IHttpActionResult Get(int patientId)
        {
            var db = new SmartChefContext();
            var query = db.PatientRepresentatives.Where(x => x.PatientId == patientId).ToList();
            return Ok(query);
        }

         
        // PUT api/PatientRepresentatives/JSON
        public void Put(PatientRepresentative patientRepresentative)
        {
            var db = new SmartChefContext();
            var query = db.PatientRepresentatives.Where(x => x.Id == patientRepresentative.Id).FirstOrDefault();
            if (query == null)
                db.PatientRepresentatives.Add(patientRepresentative);
            else
            {
                query.PatientId = patientRepresentative.PatientId;
                query.ContactNumber = patientRepresentative.ContactNumber;
                query.Email = patientRepresentative.Email;
                query.FirstName = patientRepresentative.FirstName;
                query.LastName = patientRepresentative.LastName;
                query.RelationshipWithPatient = patientRepresentative.RelationshipWithPatient;
            }
            db.SaveChanges();
        }

        // DELETE api/PatientRepresentatives/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var patientRepresentative = new PatientRepresentative { Id = id };
            ctx.PatientRepresentatives.Attach(patientRepresentative);
            ctx.PatientRepresentatives.Remove(patientRepresentative);
            ctx.SaveChanges();

        }
    }
}
