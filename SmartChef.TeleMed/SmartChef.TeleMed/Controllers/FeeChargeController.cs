﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class FeeChargeController : ApiController
    {
        
        // GET api/FeeCharges/5
        public IHttpActionResult Get(int doctorId)
        {
            var db = new SmartChefContext();
            var query = db.FeeCharges.Where(x => x.DoctorId == doctorId).ToList();

            return Ok(query);
        }

        // PUT api/FeeCharges/JSON
        public void Put(FeeCharge feeCharge)
        {
            var db = new SmartChefContext();
            var query = db.FeeCharges.Where(x => x.Id == feeCharge.Id).FirstOrDefault();
            if (query == null)
                db.FeeCharges.Add(feeCharge);
            else
            {
                
                query.DoctorId = feeCharge.DoctorId;
                query.Amount = feeCharge.Amount;
               
            }
            db.SaveChanges();
        }

        // DELETE api/FeeCharges/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var feeCharge = new FeeCharge { Id = id };
            ctx.FeeCharges.Attach(feeCharge);
            ctx.FeeCharges.Remove(feeCharge);
            ctx.SaveChanges();

        }
    }
}
