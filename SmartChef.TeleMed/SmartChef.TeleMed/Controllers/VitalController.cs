﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class VitalController : ApiController
    {
        
        // GET api/Vitals/5
        public IHttpActionResult Get(int patientId)
        {
            var db = new SmartChefContext();
            var query = db.Vitals.Where(x => x.PatientId == patientId).ToList();

            return Ok(query);
        }

         
        // PUT api/Vitals/JSON
        public void Put(Vital vital)
        {
            var db = new SmartChefContext();
            var query = db.Vitals.Where(x => x.Id == vital.Id).FirstOrDefault();
            if (query == null)
                db.Vitals.Add(vital);
            else
            {
                query.BP = vital.BP;
                query.Height = vital.Height;
                query.PatientId = vital.PatientId;
                query.RecentRecordedDate = vital.RecentRecordedDate;
                query.Sugar = vital.Sugar;
                query.Weight = vital.Weight;
               
            }
            db.SaveChanges();
        }

        // DELETE api/Vitals/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var vital = new Vital { Id = id };
            ctx.Vitals.Attach(vital);
            ctx.Vitals.Remove(vital);
            ctx.SaveChanges();

        }
    }
}
