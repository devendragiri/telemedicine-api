﻿using SmaerChef.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartChef.TeleMed.Controllers
{
  //  [Authorize]
    public class TestController : ApiController
    {
        
        // GET api/Tests/5
        public IHttpActionResult Get(int serviceId)
        {
            var db = new SmartChefContext();
            var query = db.Tests.Where(x => x.ServiceId == serviceId).ToList();
            return Ok(query);
        }

         
        // PUT api/Tests/JSON
        public void Put(Test test)
        {
            var db = new SmartChefContext();
            var query = db.Tests.Where(x => x.Id == test.Id).FirstOrDefault();
            if (query == null)
                db.Tests.Add(test);
            else
            {
                query.ServiceId= test.ServiceId;
                query.Test1 = test.Test1;
            }
            db.SaveChanges();
        }

        // DELETE api/Tests/5
        public void Delete(int id)
        {
            var ctx = new SmartChefContext();
            var test = new Test { Id = id };
            ctx.Tests.Attach(test);
            ctx.Tests.Remove(test);
            ctx.SaveChanges();

        }
    }
}
